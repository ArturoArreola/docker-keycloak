**Crear un contenedor con un servidor de autenticación Keycloak**

1. Descargar el docker image con la imagen de Keycloak
	
	`$ docker pull jboss/keycloak`

2.  Crear el contenedor con la imagen descargada (tomando en cuenta los parámetros para conectarte remotante a través de algún puerto de localhost)

`$ docker run -d -p 8080:8080 --name dockerkeycloak -e "KEYCLOAK_USER=keycloak" -e "KEYCLOAK_PASSWORD=n0t13n3!" jboss/keycloak`

> *P.E. docker run
>  			-d (para que inicie en el background)
>  			--name (para asignar un nombre al contenedor)
>  			-p [puerto en el que queremos que corra en nuestro localhost]:8080 
>  			-e "KEYCLOAK_USER=[nombre del admin del panel administrativo de keycloak]" 
>  			-e "KEYCLOAK_PASSWPRD=[contraseña del admin del panel administrativo de keycloak]*

3.  Verificar que el contenedor se haya creado correctamente

	`$ docker ps`

4.  Se puede ver el log de la creación del contenedor con el comando 

	`$ docker logs -f dockerkeycloak`
	

> *docker logs -f [nombre del contenedor]* 

5.  En caso de que se apague el equipo, se debe volver a iniciar el contenedor con el siguiente comando

`$ docker start dockerkeycloak`

> *docker start [nombre del contenedor]* 